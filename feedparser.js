/*
    FeedParser module
*/
'use strict';

var _ = require('underscore'),
    Q = require('q'),
    verror = require('verror'),
    request = require('helpers/request_wrapper').request,
    parseString = require('helpers/xml2js_wrapper').parseString,
    getFromPath = require('helpers/get_from_path');

var FeedSpecs = exports.FeedSpecs = {
    rss: {
        root: 'rss',
        lastUpdateDate: 'channel.0.pubDate.0',
        lastUpdateDate2: 'channel.0.lastBuildDate.0',
        articles: 'channel.0.item',
        articleAttributes: {
            title: 'title.0',
            link: 'link.0',
            summary: 'description.0',
            content: ['content.0', 'content:encoded.0'],
            publishedDate: ['pubDate.0', 'dc:date.0'],
            lastUpdateDate: 'pubDate.0'
        }
    },
    atom: {
        root: 'feed',
        lastUpdateDate: 'updated.0',
        articles: 'entry',
        articleAttributes: {
            title: 'title.0',
            link: 'link.0.$.href',
            summary: 'summary.0._',
            image: 'photo:imgsrc.0',
            content: 'content.0._',
            publishedDate: 'published.0',
            lastUpdateDate: 'updated.0'
        }
    }
};

var getFeedSpec = exports.getFeedSpec = function (feedJson) {
    var spec;
    _.find(FeedSpecs, function (FeedSpec) {
        if (feedJson[FeedSpec.root]) {
            spec = FeedSpec;
            return true;
        }
    });
    return spec;
};

function trim (text) {
    if (text && text.trim) {
        text = text.trim();
    }
    return text;
}

var parseFeedArticle = exports.parseFeedArticle = function (item, attributesSpec) {
    var article = _.reduce(attributesSpec.articleAttributes, function (article, attrPath, attrName) {
        var attrValue;
        if (_.isArray(attrPath)) {
            _.find(attrPath, function (path) {
                attrValue = getFromPath(item, path);
                return attrValue;
            });
        } else {
            attrValue = getFromPath(item, attrPath);
        }
        article[attrName] = trim(attrValue);
        return article;
    }, {});

    // article.link = article.link.replace(/^(https?:\/\/)(www\.)?/, '$1www.');

    // sometimes content is under attribute 'content:encoded'
    if (!article.content && article['content:encoded']) {
        article.content = trim(article['content:encoded']);
    }

    // if content is an object
    if (_.isObject(article.content) && article.content._) {
        article.content = trim(article.content._);
    }


    if (_.isObject(article.summary) && article.summary._) {
        article.summary = trim(article.summary._);
    }

    return article;
};

// function that takes xml feed content and type(rss|atom), it returns an array of articles
var parseFeedContents = exports.parseFeedContents = function (feedPage, typeOrAttributes) {
    var feed = {},
        attributes = _.isString(typeOrAttributes) ? FeedSpecs[typeOrAttributes] : typeOrAttributes;

    return parseString(feedPage).then(function (feedJson) {
        try {
            if (!attributes) {
                attributes = getFeedSpec(feedJson);
            }
            var root = feedJson[attributes.root];

            // feed main attributes: lastUpdateDate
            feed.lastUpdateDate = getFromPath(root, attributes.lastUpdateDate);
            if (!feed.lastUpdateDate && attributes.lastUpdateDate2) {
                feed.lastUpdateDate = getFromPath(root, attributes.lastUpdateDate2);
            }
            // parsing articles
            feed.articles = _.map(getFromPath(root, attributes.articles), function (item) {
                return parseFeedArticle(item, attributes);
            });
        } catch (e) {
            var error = new verror.WError(e, 'FeedParser:parseFeedContents:Feed parsing error (try/catch)');
            _.extend(error, {
                data: feedJson
            });
            return Q.reject(error);
        }
        return feed;
    });
};

exports.parseFeed = function (url, typeOrAttributes) {
    return request(url).then(function (contents) {
        contents = contents.replace(/&(?!amp;)/, '&amp;'); // replace unescaped &

        return parseFeedContents(contents, typeOrAttributes, url);
    }).fail(function (err) {
        var error = new verror.WError(err, 'FeedParser:parseFeed');
        error.url = url;
        return Q.reject(error);
    });
};
